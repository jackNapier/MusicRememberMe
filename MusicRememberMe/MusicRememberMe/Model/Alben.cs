﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace MusicRememberMe
{
	public class Alben
	{
		public string Interpret { get; set; }
		public string Titel { get; set; }
		public string HRef { get; set; }
		public string Description { get; set; }
		public string PubDate { get; set; }


		public Alben()
		{

		}
		public Alben(string line)
		{
			var split = line.Split(';');
			Interpret = split[0] ?? "";
			Titel = split[1] ?? "";
			HRef = split[2] ?? "";
			Description = split[3] ?? "";
			PubDate = split[4] ?? "";
		}

		
	}
}
