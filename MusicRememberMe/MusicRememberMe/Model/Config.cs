﻿namespace MusicRememberMe
{
	public class Config
	{
		public string ConfigOption { get; set; }
		public string Value { get; set; }

		public Config(string line)
		{
			var split = line.Split(';');
			ConfigOption = split[0] ?? "";
			Value = split[1] ?? "";
		}

	}
}