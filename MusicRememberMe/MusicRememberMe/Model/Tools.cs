﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace MusicRememberMe
{
	public class Tools
	{
		public string configFile = ".//configs//configFile.csv";

		public bool differentlyLists(List<Alben> list1, List<Alben> list2)
		{
			List<Alben> listDifference = new List<Alben>();

			foreach (var item in list2)
			{
				var result = list1.FirstOrDefault(o => o.Titel == item.Titel);
				if (result == null)
					listDifference.Add(item);
			}

			return (listDifference.Count > 0) ? true : false;
		}

		public List<Watchlist> WatchListReader(string path)
		{
			var listOfObjects = File.ReadLines(path).Skip(1).Select(line => new Watchlist(line)).ToList();
			return listOfObjects;
		}

		public List<Alben> FindListReader(string path)
		{
			var listOfObjects = File.ReadLines(path).Skip(1).Select(line => new Alben(line)).ToList();
			return listOfObjects;
		}
		public bool FileExits()
		{
			return File.Exists(configFile);
		}

		public bool FileExits(string path)
		{
			return File.Exists(path);
		}

		public void CreateWatchFile(string path)
		{
			if (FileExits(path))
				return;
			else
				using (var fs = File.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
				{
					using (TextWriter sw = new StreamWriter(fs))
					{
						string header = "Interpret;Titel";
						sw.WriteLine(header);
					}
				}
		}

		public void CreateRSSFindFile(string path)
		{
			using (var fs = File.Open(path, FileMode.OpenOrCreate, FileAccess.ReadWrite))
			{
				using (TextWriter sw = new StreamWriter(fs))
				{
					string header = "Interpret;Titel";
					sw.WriteLine(header);
				}
			}
		}

		public List<Config> ConfigReader()
		{
			var listOfObjects = File.ReadLines(configFile).Select(line => new Config(line)).ToList();
			return listOfObjects;
		}

		public List<Config> ConfigReader(string path)
		{
			var listOfObjects = File.ReadLines(path).Select(line => new Config(line)).ToList();
			return listOfObjects;
		}

		public void ConfigWriter(string path, string oldstring, string newstring)
		{
			string text = File.ReadAllText(path);
			text = text.Replace(oldstring, newstring);
			File.WriteAllText(path, text);
		}
		public void WriteLineInFile(string path, string[] line)
		{
			string text = File.ReadAllText(path);
			var str = "";
			if (line.Length == 2)
				str = line[0] + ';' + line[1];
			else
				str = line[0] + ';' + line[1] + ';' + line[2] + ';' + line[3] + ';' + line[4];
			if (text.Contains(str))
				return;
			File.AppendAllText(path, str + Environment.NewLine);
		}

		public void WriteFindList(string path, List<Alben> list)
		{
			if (!FileExits(path))
				CreateRSSFindFile(path);
			List<Alben> old = FindListReader(path);
			IEnumerable<Alben> newList = list.Except(old);
			//IEnumerable<Alben> test = list.Where(old.Exists(item2 => item2.Interpret));

			var deletedItems = list.Except(old);

			int i = newList.Count();
			foreach (var item in newList)
				WriteLineInFile(path, new string[] { item.Interpret, item.Titel, item.HRef, item.Description, item.PubDate });
		}
	}
}
