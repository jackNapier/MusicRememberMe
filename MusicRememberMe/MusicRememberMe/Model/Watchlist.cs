﻿namespace MusicRememberMe
{
	public class Watchlist
	{
		public string Interpret { get; set; }
		public string Titel { get; set; }

		public Watchlist(string line)
		{
			var split = line.Split(';');
			Interpret = split[0] ?? "";
			Titel = split[1] ?? "";
		}
	}
}
