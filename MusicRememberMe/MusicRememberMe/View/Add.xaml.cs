﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace MusicRememberMe.View
{
	/// <summary>
	/// Interaktionslogik für Add.xaml
	/// </summary>
	public partial class Add : Window
	{
		private List<Watchlist> watchlist = new List<Watchlist>();
		private Tools tools = new Tools();
		private List<Config> config = new List<Config>();
		public Add()
		{
			InitializeComponent();
			config = tools.ConfigReader();
			LoadWatchlist(config);
		}

		private void LoadWatchlist(List<Config> config)
		{
			watchlist = tools.WatchListReader(config[0].Value);
			lstAlben.ItemsSource = null;
			lstAlben.ItemsSource = watchlist;
		}
		private void btnOK_Click(object sender, RoutedEventArgs e)
		{
			DialogResult = true;
		}

		private void btnAdd_Click(object sender, RoutedEventArgs e)
		{
			tools.WriteLineInFile(config[0].Value, new string[] { txtInterpret.Text,""});
			LoadWatchlist(config);
		}
	}
}
