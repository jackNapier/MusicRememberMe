﻿using System.Collections.Generic;
using System.Windows;
using System.Windows.Forms;

namespace MusicRememberMe
{
	/// <summary>
	/// Interaktionslogik für Options.xaml
	/// </summary>
	public partial class Options : Window
	{
		public List<Config> config = new List<Config>();
		private Tools tools = new Tools();
		public string configFile = ".//configs//configFile.csv";
		private string TimerTemp,WatchTemp,RSSTemp;

		public Options()
		{
			InitializeComponent();
			config = tools.ConfigReader(configFile);
			WatchListPath.Text = WatchTemp = config[0].Value;
			RssListPath.Text = RSSTemp= config[1].Value;
			Timer.Text = TimerTemp = config[2].Value;
		}

		private void Ok_Click(object sender, RoutedEventArgs e)
		{
			tools.CreateWatchFile(WatchListPath.Text);
			tools.ConfigWriter(configFile, WatchTemp , WatchListPath.Text);

			tools.CreateRSSFindFile(RssListPath.Text);
			tools.ConfigWriter(configFile,RSSTemp, RssListPath.Text);

			tools.ConfigWriter(configFile, TimerTemp, Timer.Text);

			DialogResult = true;
		}

		private void btnWatchList_Click(object sender, RoutedEventArgs e)
		{
			SaveFileDialog savefile = new SaveFileDialog();
			// set a default file name
			savefile.FileName = "watchlist.csv";
			// set filters - this can be done in properties as well
			savefile.Filter = "CSV files (*.csv)|*.*";

			if (savefile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				
				WatchListPath.Text = savefile.FileName;
			}
		}

		private void btnRssList_Click(object sender, RoutedEventArgs e)
		{
			SaveFileDialog savefile = new SaveFileDialog();
			// set a default file name
			savefile.FileName = "Find-List.csv";
			// set filters - this can be done in properties as well
			savefile.Filter = "CSV files (*.csv)|*.*";

			if (savefile.ShowDialog() == System.Windows.Forms.DialogResult.OK)
			{
				RssListPath.Text = savefile.FileName;
			}
		}
	}
}
