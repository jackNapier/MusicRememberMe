﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Text;
using System.Windows;
using System.Windows.Threading;
using System.Xml;

namespace MusicRememberMe
{
	/// <summary>
	/// Interaktionslogik für MainWindow.xaml
	/// </summary>
	public partial class MainWindow : System.Windows.Window
	{
		private List<Watchlist> watchList = new List<Watchlist>();
		public List<Config> config = new List<Config>();
		private List<Alben> list = new List<Alben>();
		private Tools tools = new Tools();

		public string csvpath = "";

		public MainWindow()
		{
			InitializeComponent();
			if (!tools.FileExits())
			{
				ShowForm();
			}
			config = tools.ConfigReader();
			MinimizeToTray.Enable(this);
			DispatcherTimer timer = new DispatcherTimer();
			timer.Interval = TimeSpan.FromSeconds(int.Parse(config[2].Value));
			timer.Tick += timer_Tick;
			timer.Start();
			if (!tools.FileExits(config[0].Value))
				tools.CreateWatchFile(config[0].Value);
			watchList = tools.WatchListReader(config[0].Value);
		}
		private void Hyperlink_RequestNavigate(object sender, System.Windows.Navigation.RequestNavigateEventArgs e)
		{
			System.Diagnostics.Process.Start(e.Uri.AbsoluteUri);
		}

		public List<Alben> Search(string path)
		{
			if (cbWatchList.IsChecked != true)
			{
				watchList = new List<Watchlist>();
				watchList.Add(new Watchlist("*;*"));

			}
			else
			{
				watchList = tools.WatchListReader(path);
				WatchItems.Text = "Items in Watchlist: " + watchList.Count;
			}

			var rssXmlDoc = new XmlDocument();
			// Load the RSS file from the RSS URL
			rssXmlDoc.Load("http://ddl-music.to/rss/alben.xml");
			// Parse the Items in the RSS file
			var rssNodes = rssXmlDoc.SelectNodes("rss/channel/item");
			List<Alben> alben = new List<Alben>();
			// Iterate through the items in the RSS file
			foreach (var s in watchList)
			{
				foreach (XmlNode rssNode in rssNodes)
				{
					var item = new Alben();
					var iso = Encoding.GetEncoding("ISO-8859-1");
					var rssSubNode = rssNode.SelectSingleNode("title");
					item.Interpret = Encoding.UTF8.GetString(iso.GetBytes(rssSubNode != null ? rssSubNode.InnerText.Substring(0, rssSubNode.InnerText.IndexOf(" - ")) : ""));
					if (!(item.Interpret.ToLower().Contains(s.Interpret.ToLower())) && s.Interpret.ToLower() != "*")
						continue;
					item.Titel = Encoding.UTF8.GetString(iso.GetBytes(rssSubNode.InnerText.Substring(rssSubNode.InnerText.IndexOf("- ") + 2)));
					rssSubNode = rssNode.SelectSingleNode("link");
					item.HRef = Encoding.UTF8.GetString(iso.GetBytes(rssSubNode != null ? rssSubNode.InnerText : ""));

					rssSubNode = rssNode.SelectSingleNode("description");
					item.Description = Encoding.UTF8.GetString(iso.GetBytes(rssSubNode != null ? rssSubNode.InnerText : ""));

					rssSubNode = rssNode.SelectSingleNode("pubDate");
					var dt = DateTime.Parse(Encoding.UTF8.GetString(iso.GetBytes(rssSubNode != null ? rssSubNode.InnerText : "")), new CultureInfo("de-DE"));
					item.PubDate = dt.ToString();

					
					alben.Add(item);
				}
			}

			if (tools.differentlyLists(list, alben))
			{
				list = alben;
				lstAlben.ItemsSource = null;
				lstAlben.ItemsSource = list;
				Items.Text = "Items: " + list.Count;

				MessageBox.Show("List updated: " + DateTime.Now.ToString("HH:mm:ss"));
				tools.WriteFindList(config[1].Value, list);
				return list;
			}
			else
				return list;
		}

		private void btnSearch_Click(object sender, RoutedEventArgs e)
		{
			Search(config[0].Value);
		}

		private void timer_Tick(object sender, EventArgs e)
		{
			Search(config[0].Value);
		}

		private void CheckBoxChanged(object sender, RoutedEventArgs e)
		{
			list = new List<Alben>();
			WatchItems.Text = "";
		}

		private void Options_Click(object sender, RoutedEventArgs e)
		{
			ShowForm();
		}

		private void ShowForm()
		{
			Options win2 = new Options();

			if (win2.ShowDialog() == win2.DialogResult)
			{
				config = tools.ConfigReader();
				watchList = tools.WatchListReader(config[0].Value);
				WatchItems.Text = "Items in Watchlist: " + watchList.Count;
			}
		}

		private void AddToWatchlist_Click(object sender, RoutedEventArgs e)
		{
			View.Add win2 = new View.Add();

			if (win2.ShowDialog() == win2.DialogResult)
			{
				watchList = tools.WatchListReader(config[0].Value);
				WatchItems.Text = "Items in Watchlist: " + watchList.Count;
			}
		}
	}

}
